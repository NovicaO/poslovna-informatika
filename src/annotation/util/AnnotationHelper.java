package annotation.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import annotation.Column;
import annotation.ForeignKey;
import annotation.ID;
import annotation.Table;
import util.Utils;

public class AnnotationHelper {

	public static String[] getColumnNames(Class<?> c) {
		List<String> column_names = new ArrayList<String>();

		Class<?> aClass = (Class<?>) c;
		Field[] fields = aClass.getDeclaredFields();
		for (Field field : fields) {
			Annotation[] annotations = field.getDeclaredAnnotations();
			for (Annotation annotation : annotations)
				if (annotation instanceof Column) {
					Column myAnnotation = (Column) annotation;
					column_names.add(myAnnotation.name().split("name: ")[0]);
				}
		}
		
		return Utils.listToArray(column_names);
	}

	public static String getId(Class<?> c) {
		String id_name = "";

		Class<?> aClass = (Class<?>) c;
		Field[] fields = aClass.getDeclaredFields();
		for (Field field : fields) {
			Annotation[] annotations = field.getDeclaredAnnotations();
			for (Annotation annotation : annotations)
				if (annotation instanceof ID) {
					ID myAnnotation = (ID) annotation;
					id_name = myAnnotation.name().split("name: ")[0];
					break;
				}
		}
		return id_name;
	}
	
	public static String getForeignKey(Class<?> c) {
		String foreignKeyName = "";

		Class<?> aClass = (Class<?>) c;
		Field[] fields = aClass.getDeclaredFields();
		for (Field field : fields) {
			Annotation[] annotations = field.getDeclaredAnnotations();
			for (Annotation annotation : annotations)
				if (annotation instanceof ForeignKey) {
					ForeignKey myAnnotation = (ForeignKey) annotation;
					foreignKeyName = myAnnotation.name().split("name: ")[0];
					break;
				}
		}
		return foreignKeyName;
	}

	public static String getTableName(Class<?> c) {
		String table_name = "";

		Class<?> aClass = (Class<?>) c;
		Annotation[] annotations = aClass.getAnnotations();
		for (Annotation annotation : annotations)
			if (annotation instanceof Table) {
				Table myAnnotation = (Table) annotation;
				table_name = myAnnotation.name().split("name: ")[0];
				break;
			}
		return table_name;
	}

}
