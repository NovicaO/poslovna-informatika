package gui.dialog;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import annotation.util.AnnotationHelper;
import database.BusinessPartnerTableModel;
import database.CatalogItemTableModel;
import gui.MainFrame;
import model.bean.BusinessPartner;
import model.bean.CatalogItem;
import model.bean.Data;
import net.miginfocom.swing.MigLayout;
import util.ColumnList;
import util.Lookup;
import util.Utils;

public class CatalogItemStandardForm extends JDialog {
	private static final long serialVersionUID = 1L;

	public static final int MODE_EDIT = 1;
	public static final int MODE_ADD = 2;
	public static final int MODE_SEARCH = 3;
	private int mode;

	private JToolBar toolBar;
	private JButton btnAdd, btnCommit, btnDelete, btnFirst, btnLast, btnHelp,
			btnNext, btnNextForm, btnPickup, btnRefresh, btnRollback,
			btnSearch, btnPrevious;
	
	private JTable tblGrid;
	
	public CatalogItemTableModel tableModel;
	private JLabel labelMode = new JLabel("");
	private JTextField tfCatalodName = new JTextField(20);
	private JTextField tfIdCatalog = new JTextField(10);
	public ColumnList columnLista;

	public ColumnList getColumnLista() {
		return columnLista;
	}

	public void setColumnLista(ColumnList columnLista) {
		this.columnLista = columnLista;
	}

	private JButton btnZoom = new JButton("...");
	private JTextField tfPrice = new JTextField(10);
	private JTextField tfID = new JTextField(10);
	private JLabel lblNewLabel;
	
	public CatalogItemStandardForm(ColumnList col) {
		columnLista= col;
		getContentPane().setLayout(new MigLayout("fill", "[]", "[][][][]"));
		setSize(new Dimension(800, 600));
		setTitle("Business Partner");
		setLocationRelativeTo(MainFrame.getInstance());
		setModal(true);
		initToolbar();
		try {
			initTable();
			tblGrid.requestFocus();
			tblGrid.changeSelection(0,0,false, false);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		initGui();
		labelMode.setText("EDIT");
		setMode(MODE_EDIT);
		tfID.setEditable(false);
		//btnZoom.setEnabled(false);
		tfIdCatalog.setEditable(false);
		tfID.setEditable(false);


		if (col != null) {
			tfCatalodName.setText((String) col.getValue("grupa_naziv"));
			tfIdCatalog.setText((String) col.getValue("podgrupa.podgrupa_id"));

			tfCatalodName.setEditable(false);
			tfIdCatalog.setEditable(false);

		}

		tfIdCatalog.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				String sifraDrzave = tfIdCatalog.getText().trim();
				try {
					tfCatalodName.setText(Lookup.getCompanyName(sifraDrzave));
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

	}

	private void initGui() {
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout(new MigLayout("", "[][grow][][][][][grow][][][]", "[][][]"));

		JPanel buttonsPanel = new JPanel();
		btnCommit = new JButton(new ImageIcon(getClass().getResource(
				"/img/commit.gif")));
		btnCommit.addActionListener(commitAction());
		btnRollback = new JButton(new ImageIcon(getClass().getResource(
				"/img/remove.gif")));
		btnRollback.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		JLabel lbCatalogId = new JLabel("Catalog ID:");
		
		lblNewLabel = new JLabel("ID:");
		dataPanel.add(lblNewLabel, "cell 0 0,alignx trailing");
		

		dataPanel.add(tfID, "cell 1 0,growx");
		tfID.setColumns(10);
		JLabel lbIdCatalogItem = new JLabel("Price");
		dataPanel.add(lbIdCatalogItem, "cell 2 0,alignx left");
		
	

		dataPanel.add(tfPrice, "cell 3 0,growx");
		tfPrice.setColumns(10);
		dataPanel.add(lbCatalogId, "cell 0 2");
		dataPanel.add(tfIdCatalog, "cell 1 2,gapx 15px");
		
		
		
		labelMode.setFont(new Font("Tahoma", Font.BOLD, 30));
		getContentPane().add(labelMode, "cell 0 1,alignx center");
		dataPanel.add(btnZoom, "cell 2 2");
		
				btnZoom.addActionListener(new ActionListener() {
		
					@Override
					public void actionPerformed(ActionEvent arg0) {
		
						CatalogStandardForm csf = new CatalogStandardForm();
						csf.setVisible(true);
						ColumnList columnList = csf.getColumnLista();
						if (columnList != null) {
							tfCatalodName.setText((String) columnList
									.getValue("name"));
							tfIdCatalog.setText((String) columnList
									.getValue("id"));
						}
					}
				});
		bottomPanel.add(dataPanel);
				
						dataPanel.add(tfCatalodName, "cell 3 2,pushx ");
						tfCatalodName.setEditable(false);

		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel, "dock east");

		getContentPane().add(bottomPanel, "cell 0 2,grow");

	}

	private void initToolbar() {
		toolBar = new JToolBar();

		btnSearch = new JButton(new ImageIcon(getClass().getResource(
				"/img/search.gif")));

		btnSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnSearch);

		btnRefresh = new JButton(new ImageIcon(getClass().getResource(
				"/img/refresh.gif")));

		btnRefresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnRefresh);

		btnPickup = new JButton(new ImageIcon(getClass().getResource(
				"/img/zoom-pickup.gif")));
		btnPickup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnPickup);

		btnHelp = new JButton(new ImageIcon(getClass().getResource(
				"/img/help.gif")));
		btnHelp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnHelp);

		toolBar.addSeparator();

		btnFirst = new JButton(new ImageIcon(getClass().getResource(
				"/img/first.gif")));
		btnFirst.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnFirst);

		btnPrevious = new JButton(new ImageIcon(getClass().getResource(
				"/img/prev.gif")));
		btnPrevious.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		toolBar.add(btnPrevious);

		btnNext = new JButton(new ImageIcon(getClass().getResource(
				"/img/next.gif")));
		btnNext.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		toolBar.add(btnNext);

		btnLast = new JButton(new ImageIcon(getClass().getResource(
				"/img/last.gif")));
		btnLast.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		toolBar.add(btnLast);

		toolBar.addSeparator();

		btnAdd = new JButton(new ImageIcon(getClass().getResource(
				"/img/add.gif")));
		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setMode(MODE_ADD);
				labelMode.setText("ADD");
				tfCatalodName.setText("");
				tfIdCatalog.setText("");
				tfID.setText("");
				tfPrice.setText("");
				btnZoom.setEnabled(true);
				tfID.setEditable(false);
				tfIdCatalog.setEditable(false);
			}
		});

		toolBar.add(btnAdd);

		btnDelete = new JButton(new ImageIcon(getClass().getResource(
				"/img/remove.gif")));
		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog.setDefaultLookAndFeelDecorated(true);
				int response = JOptionPane
						.showConfirmDialog(null, "Do you want to continue?",
								"Confirm", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.NO_OPTION) {

				} else if (response == JOptionPane.YES_OPTION) {
					int selected = tblGrid.getSelectedRow();
					try {
						tableModel.deleteRow(selected);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					tblGrid.requestFocus();
					tblGrid.changeSelection(tableModel.getRowCount()-1,tableModel.getRowCount()-1,false, false);
					
				} else if (response == JOptionPane.CLOSED_OPTION) {

				}

			}
		});
		toolBar.add(btnDelete);

		toolBar.addSeparator();

		btnNextForm = new JButton(new ImageIcon(getClass().getResource(
				"/img/nextform.gif")));
		btnNextForm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		toolBar.add(btnNextForm);

		btnPickup.setEnabled(false);
		getContentPane().add(toolBar, "dock north");

	}

	private void initTable() throws SQLException {
		tblGrid= new JTable();
		JScrollPane scrollPane = new JScrollPane(tblGrid);
		getContentPane().add(scrollPane, "cell 0 0,grow");

		String[] column_names = AnnotationHelper.getColumnNames(CatalogItem.class);
		String table_name = AnnotationHelper.getTableName(CatalogItem.class);
		String id_column_name = AnnotationHelper.getId(CatalogItem.class);
		tableModel = new CatalogItemTableModel(column_names, 0, table_name, id_column_name);
		tblGrid.setModel(tableModel);
		if (columnLista != null) {
			//tableModel.openAsChildForm(columnLista.getWhereClause());
		} else {
			try {
				tableModel.open();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		// Dozvoljeno selektovanje redova
		tblGrid.setRowSelectionAllowed(true);
		// Ali ne i selektovanje kolona
		tblGrid.setColumnSelectionAllowed(false);

		// Dozvoljeno selektovanje samo jednog reda u jedinici vremena
		tblGrid.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		tblGrid.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent e) {
						if (e.getValueIsAdjusting())
							return;
						sync();
					}
				});

	}
	private ActionListener commitAction() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mode == MODE_EDIT) {	
					int selected = tblGrid.getSelectedRow();
					try {
						LinkedList<Data> values = new LinkedList<Data>();
					  // values.add(new Data("id_business_partner", getTFText(tfSifraGrupe)));
						values.add(new Data("id_catalog", getTFText(tfIdCatalog)));
						values.add(new Data("price", getTFText(tfPrice)));
						tableModel.updateRow(values, selected);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					tblGrid.setRowSelectionInterval(selected, selected); // TODO: check this
				} else if (mode == MODE_ADD) 
					addRow();
				 else if (mode == MODE_SEARCH) {
					//tableModel.search(tfSifra.getText(), tfName.getText()); //TODO commented out cuz idk what does it do
					//setMode(MODE_EDIT);
				}

			}

			private String getTFText(JTextField tf) {
				return tf.getText().trim();
			}
		};
	}

	private void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) {
			tfID.setText("");
			tfIdCatalog.setText("");
			tfPrice.setText("");
			return;
		}
		String id = (String) tableModel.getValueAt(index, 0);
		String catalogId = (String) tableModel.getValueAt(index, 1);
		String price = (String) tableModel.getValueAt(index, 2);
		tfID.setText(id);
		tfIdCatalog.setText(catalogId);
		tfPrice.setText(price);
		setMode(MODE_EDIT);
		labelMode.setText("EDIT");
		tfID.setEditable(false);
		tfIdCatalog.setEditable(false);
		try {
			tfCatalodName.setText(Lookup.getCatalogName(catalogId));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//	btnZoom.setEnabled(false);
	}
	private void addRow() {
		//String idSubGroup = tfPodgrupa.getText().trim();
		String ID = tfIdCatalog.getText().trim();
		String price = tfPrice.getText().trim();
		LinkedList<Data> values= new LinkedList<Data>();
		//values.add(new Data("id_group", name));
		values.add(new Data("id_catalog", ID));
		values.add(new Data("price", price));
		try {
			int index = tableModel.insertRow(values);
			tableModel.insertRow(tableModel.getRowCount(), Utils.linkedListToArray(index,values));
			tblGrid.requestFocus();
			tblGrid.changeSelection(tableModel.getRowCount()-1,tableModel.getRowCount()-1,false, false);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void setMode(int mode) {
		this.mode = mode;
	}

}
