package gui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import annotation.util.AnnotationHelper;
import database.CatalogItemTableModel;
import database.DBConnection;
import gui.MainFrame;
import model.bean.CatalogItem;
import net.miginfocom.swing.MigLayout;
import util.ColumnList;

public class BillAddingStandardForm extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private JTextField tfBusinessYear;
	private JComboBox<String> cb_invoiceType;
	private JLabel lblType;
	private JLabel lblBusinessYear;
	private JLabel lblNewLabel;
	private JTextField tfDateValue;
	private JTextField tfPoA_1;
	private JTextField tfPoA_2;
	private JTextField tfPoA_3;
	private JTextField tfPoA_4;
	private JLabel lblCallNumber;
	private JTextField tfCallNumber;
	private JScrollPane spCatalogItems;
	private JLabel lblCatalogItems;
	private JLabel lblAddedItems;
	private JScrollPane spAddedItems;
	private JTable tableCatalogItems;
	private JTable tableAddedItems;
	private CatalogItemTableModel tmCatalogItem;
	private CatalogItemTableModel tmAddedCatalogItem;
	private JButton btnAddCatalogItem;
	private JButton btnRemoveAddedItem;
	private JButton btnCreateInvoice;
	private JButton btnBusinessPartner;
	private JTextField tfBusinessPartner;
	private JTextField tfBusinessPartnerId;
	
	@SuppressWarnings("unused")
	private class AddedItem{
		int item_id;
		int catalog_id;
		double price;
		
		AddedItem(int item_id, int catalog_id, double price){
			this.item_id    = item_id;
			this.catalog_id = catalog_id;
			this.price      = price;
		}
	}
	
	//Constructor
	public BillAddingStandardForm() {
		setSize(new Dimension(560, 600));
		setTitle("Add Invoice");
		setLocationRelativeTo(MainFrame.getInstance());
		setModal(true);
		getContentPane().setLayout(new MigLayout("", "[grow]", "[][][][][][][][][][][][][][][][]"));
		
		lblBusinessYear = new JLabel("Business year");
		getContentPane().add(lblBusinessYear, "cell 0 0");
		lblType = new JLabel("Type");
		getContentPane().add(lblType, "cell 0 2");
		
		tfBusinessYear = new JTextField();
		tfBusinessYear.setEditable(false);
		tfBusinessYear.setColumns(4);
		getContentPane().add(tfBusinessYear, "flowx,cell 0 1");
		
		cb_invoiceType = new JComboBox<>();
		getContentPane().add(cb_invoiceType, "cell 0 3");
		
		lblNewLabel = new JLabel("Date value (datum valute)");
		getContentPane().add(lblNewLabel, "cell 0 4");
		
		tfDateValue = new JTextField();
		tfDateValue.setEditable(false);
		tfDateValue.setColumns(6);
		getContentPane().add(tfDateValue, "cell 0 5");
		
		tfPoA_1 = new JTextField();
		getContentPane().add(tfPoA_1, "flowx,cell 0 7");
		tfPoA_1.setColumns(4);
		tfPoA_1.addKeyListener(getPoAKeyAdapter(tfPoA_1));
		
	
		tfPoA_2 = new JTextField();
		tfPoA_2.setColumns(4);
		getContentPane().add(tfPoA_2, "cell 0 7");
		tfPoA_2.addKeyListener(getPoAKeyAdapter(tfPoA_2));
		
		tfPoA_3 = new JTextField();
		tfPoA_3.setColumns(4);
		getContentPane().add(tfPoA_3, "cell 0 7");
		tfPoA_3.addKeyListener(getPoAKeyAdapter(tfPoA_3));
		
		tfPoA_4 = new JTextField();
		tfPoA_4.setColumns(4);
		getContentPane().add(tfPoA_4, "cell 0 7");
		tfPoA_4.addKeyListener(getPoAKeyAdapter(tfPoA_4));
		
		
		lblCallNumber = new JLabel("Call number:");
		getContentPane().add(lblCallNumber, "cell 0 7");
		
		tfCallNumber = new JTextField();
		getContentPane().add(tfCallNumber, "cell 0 7");
		tfCallNumber.setColumns(20);
		tfCallNumber.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				tfLetterConstraint(e);
				tfLengthConstraint(e, tfCallNumber, 20);
			}
		});
		
		lblCatalogItems = new JLabel("Catalog items");
		getContentPane().add(lblCatalogItems, "cell 0 10");
		
		lblAddedItems = new JLabel("Added items");
		getContentPane().add(lblAddedItems, "cell 0 13");
		
		//Catalog items
		tableCatalogItems = new JTable();
		spCatalogItems    = new JScrollPane(tableCatalogItems);
		getContentPane().add(spCatalogItems, "flowx,cell 0 11");
		
		String[] column_names = AnnotationHelper.getColumnNames(CatalogItem.class);
		String table_name     = AnnotationHelper.getTableName(CatalogItem.class);
		String id_column_name = AnnotationHelper.getId(CatalogItem.class);
		tmCatalogItem         = new CatalogItemTableModel(column_names, 0, table_name, id_column_name);
		tableCatalogItems.setModel(tmCatalogItem);
		try {
			tmCatalogItem.open("SELECT * FROM poslovna_informatika.catalog_item WHERE id_catalog = (SELECT id_catalog FROM poslovna_informatika.catalog WHERE acting_date < now() ORDER BY acting_date DESC LIMIT 1)");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//Added items
		tableAddedItems   = new JTable();
		spAddedItems      = new JScrollPane(tableAddedItems);
		getContentPane().add(spAddedItems, "flowx,cell 0 14");
		
		column_names       = AnnotationHelper.getColumnNames(CatalogItem.class);
		table_name         = AnnotationHelper.getTableName(CatalogItem.class);
		id_column_name     = AnnotationHelper.getId(CatalogItem.class);
		tmAddedCatalogItem = new CatalogItemTableModel(column_names, 0, table_name, id_column_name);
		tableAddedItems.setModel(tmAddedCatalogItem);
		
		btnAddCatalogItem = new JButton("Add");
		getContentPane().add(btnAddCatalogItem, "cell 0 11");
		btnAddCatalogItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					int index = tableCatalogItems.getSelectedRow();
	
					Integer id_item         = Integer.valueOf((String) tableCatalogItems.getValueAt(index, 0));
					Integer id_catalog_item = Integer.valueOf((String) tableCatalogItems.getValueAt(index, 1));
					Double  item_price      = Double.valueOf((String)  tableCatalogItems.getValueAt(index, 2));
					
					((CatalogItemTableModel) tableAddedItems.getModel()).addRow(new Object[]{id_item, id_catalog_item, item_price});
				}
				catch(Exception exc){
					//User probably didn't select a row - ignore it
				}
			}
		});
		
		btnRemoveAddedItem = new JButton("Remove");
		getContentPane().add(btnRemoveAddedItem, "cell 0 14");
		
		
		btnRemoveAddedItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					int index = tableAddedItems.getSelectedRow();
					((CatalogItemTableModel) tableAddedItems.getModel()).removeRow(index);
				}
				catch(Exception exc){
					//User probably didn't select a row - ignore it
				}
			}
		});
		
		
		btnCreateInvoice = new JButton("Create Invoice");
		btnCreateInvoice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				CatalogItemTableModel tm = (CatalogItemTableModel) tableAddedItems.getModel();
				int rowCount = tm.getRowCount();
				
				List<AddedItem> addedItems = new ArrayList<AddedItem>();
				
				for(int i=0; i<rowCount; i++)
					addedItems.add(new AddedItem((int) tm.getValueAt(i, 0), (int) tm.getValueAt(i, 1), (double) tm.getValueAt(i, 2)));
				
				/*
				 1.Calculate total price
				 2.Calculate total product number
				 3.Calculate total taxes
				 .
				 .
				 .
				 n.Create invoice based on all the data
				 */
				
				// 1.Calculate total price
				Double totalPrice = 0d;
				for(AddedItem ai : addedItems)
					totalPrice += ai.price;
				
				// 2.Calculate total product number
				int totalProductNumber = addedItems.size();

				// 3.Calculate total taxes
				double totalTaxes = totalPrice + (totalPrice/100) * getTaxRate();
				
				// 4. Business year
				int businessYear = new Integer(tfBusinessYear.getText());
				
				// 5. Status
				char status = 'C'; //Created
				
				// 7. Payment on account
				String PoA = tfPoA_1.getText() + tfPoA_2.getText() + tfPoA_3.getText() + tfPoA_4.getText();
				
				// 8. Call number
				String callNumber = tfCallNumber.getText();
				
				// 9. Type
				String type = cb_invoiceType.getSelectedItem().toString();
				
				createInvoice(addedItems, totalPrice, totalProductNumber, totalTaxes, businessYear, status, PoA, callNumber, type);
			}
		});
		getContentPane().add(btnCreateInvoice, "cell 0 15,alignx center");
		
		btnBusinessPartner = new JButton("Business Partner");
		getContentPane().add(btnBusinessPartner, "cell 0 1");
		
		tfBusinessPartner = new JTextField();
		tfBusinessPartner.setEditable(false);
		getContentPane().add(tfBusinessPartner, "cell 0 1");
		tfBusinessPartner.setColumns(10);
		
		tfBusinessPartnerId = new JTextField();
		tfBusinessPartnerId.setEditable(false);
		tfBusinessPartnerId.setEnabled(false);
		getContentPane().add(tfBusinessPartnerId, "cell 0 1");
		tfBusinessPartnerId.setColumns(1);
		tfBusinessPartnerId.setVisible(false);
		btnBusinessPartner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BusinessPartnerStandardForm bpsf = new BusinessPartnerStandardForm();
				bpsf.setVisible(true);
				ColumnList cl = bpsf.getColumnLista();
				tfBusinessPartner.setText(cl.getValue("name").toString());
				tfBusinessPartnerId.setText(cl.getValue("ID").toString());
				System.out.println(tfBusinessPartnerId.getText());
			}
		});
		
		//Initialize the data
		init();
	}

	public void init(){
		init_businessYear();
		init_invoiceType();
		init_dateValue();
	}
	
	private void init_dateValue() {
		Calendar currentDate = Calendar.getInstance();
		
		int currentDay   = currentDate.get(Calendar.DAY_OF_MONTH);
		int currentMonth = currentDate.get(Calendar.MONTH) + 1;
		int currentYear  = currentDate.get(Calendar.YEAR);
		tfDateValue.setText(currentDay+"/"+currentMonth+"/"+currentYear);
	}

	private void init_invoiceType(){
		cb_invoiceType.addItem("Type 1");
		cb_invoiceType.addItem("Type 2");
		cb_invoiceType.addItem("Type 3");
	}
	
	private boolean conditionsMet(){
		return (tfBusinessPartner.getText().length() > 1)
				&& tfPoA_1.getText().length() > 1
				&& tfPoA_2.getText().length() > 1
				&& tfPoA_3.getText().length() > 1
				&& tfPoA_4.getText().length() > 1
				&& ((CatalogItemTableModel) tableAddedItems.getModel()).getRowCount() != 0;
	}
	
	private void createInvoice(List<AddedItem> addedItems, double totalPrice, int totalProductNumber, double totalTaxes, int businessYear, char status, String PoA, String callNumber, String type) {
		
		if(!conditionsMet())
			return;
		
		int last_id        = getLastBillNumber();
		String value_names = "bill_number, business_year, type, value_date, tax_total, price_sum, payment_on_account, call_number, status, business_partner, product_total";
		String query       = "INSERT INTO poslovna_informatika.bill ("+value_names+") VALUES (?, ?, ?, now(), ?, ?, ?, ?, ?, ?, ?)";
		
		try{
			PreparedStatement stmt = DBConnection.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			
			stmt.setObject(1,  ++last_id);
			stmt.setObject(2,  businessYear);
			stmt.setObject(3,  type);
			stmt.setObject(4,  totalTaxes);
			stmt.setObject(5,  totalPrice);
			stmt.setObject(6,  PoA);
			stmt.setObject(7,  callNumber);
			stmt.setObject(8,  String.valueOf(status));
			stmt.setObject(9,  tfBusinessPartnerId.getText()); 
			stmt.setObject(10, totalProductNumber);
			
			stmt.executeUpdate();
			DBConnection.getConnection().commit();
			
			//Add items to bill_item
			Map<Integer, Integer> bill_items = new HashMap<Integer, Integer>(); //Integer1 = item_id,  Integer2 = amount
			
			for(AddedItem ai : addedItems)
				if(bill_items.containsKey(ai.item_id)){
					int previousAmount = bill_items.get(ai.item_id);
					bill_items.replace(ai.item_id, ++previousAmount);
				}
				else
					bill_items.put(ai.item_id, 1);
			
			createInvoiceitems(bill_items, last_id);
			
			System.out.println("Invoice created!");
			
			stmt.close();
			
			dispose();
		}catch(Exception e){e.printStackTrace();}
	}
	
	private void createInvoiceitems(Map<Integer, Integer> bill_items, int bill_number) {
		String value_names = "item_number, amount, bill_number";
		String query       = "INSERT INTO poslovna_informatika.bill_items("+value_names+") VALUES (?, ?, ?)";
		
		for (Integer item : bill_items.keySet()) {
			try {
				PreparedStatement stmt = DBConnection.getConnection().prepareStatement(query);

				stmt.setObject(1, item);
				stmt.setObject(2, bill_items.get(item));
				stmt.setObject(3, bill_number);

				stmt.executeUpdate();
				DBConnection.getConnection().commit();
			} catch (Exception e) {e.printStackTrace();}
		}
	}

	private int getLastBillNumber(){
		int lastBillNumber = 0;
		try{
			Statement stmt = DBConnection.getConnection().createStatement();
			ResultSet rset = stmt.executeQuery("SELECT bill_number FROM poslovna_informatika.bill ORDER BY bill_number ASC");
			while (rset.next())
				lastBillNumber = rset.getInt("bill_number");
			rset.close();
			stmt.close();
		}catch(Exception e){e.printStackTrace();}
		return lastBillNumber;
	}
	
	private double getTaxRate(){
		//SELECT * FROM poslovna_informatika.tax_rate WHERE id_tax = (SELECT id_tax FROM poslovna_informatika.tax WHERE effective = 1 LIMIT 1);
		double taxRate = 0;
		try{
			Statement stmt = DBConnection.getConnection().createStatement();
			ResultSet rset = stmt.executeQuery("SELECT * FROM poslovna_informatika.tax_rate WHERE id_tax = (SELECT id_tax FROM poslovna_informatika.tax WHERE effective = 1 LIMIT 1);");
			while (rset.next())
				taxRate = rset.getDouble("rate");
			rset.close();
			stmt.close();
		}catch(Exception e){e.printStackTrace();}
		return taxRate;
	}
	
	private void init_businessYear(){
		String businessYear = "";
		try{
			Statement stmt = DBConnection.getConnection().createStatement();
			ResultSet rset = stmt.executeQuery("SELECT * FROM poslovna_informatika.business_year WHERE concluded != 1 ORDER BY YEAR ASC LIMIT 1");
			while (rset.next())
				businessYear = rset.getString("year");
			rset.close();
			stmt.close();
		}catch(Exception e){e.printStackTrace();}
		tfBusinessYear.setText(businessYear);
	}
	
	private KeyAdapter getPoAKeyAdapter(final JTextField tf){
		return new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				tfConstraints(e, tf);
			}
		};
	}
	
	private void tfConstraints(KeyEvent e, JTextField tf) {
		tfLetterConstraint(e);
		tfLengthConstraint(e, tf, 4);
	}

	private void tfLengthConstraint(KeyEvent e, JTextField tf, int allowedLength) {
		if (tf.getText().length() >= allowedLength) 
			e.consume();
	}

	private void tfLetterConstraint(KeyEvent e) {
		char c = e.getKeyChar();
		if (((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE))
			e.consume();
	}
}