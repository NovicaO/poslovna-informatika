package gui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import annotation.util.AnnotationHelper;
import database.SubGroupTableModel;
import database.TaxRateTableModel;
import gui.MainFrame;
import model.bean.Data;
import model.bean.Subgroup;
import model.bean.TaxRate;
import net.miginfocom.swing.MigLayout;
import util.ColumnList;
import util.Lookup;
import util.Utils;

import java.awt.Font;

public class TaxRateStandardForm extends JDialog {
	private static final long serialVersionUID = 1L;

	public static final int MODE_EDIT = 1;
	public static final int MODE_ADD = 2;
	public static final int MODE_SEARCH = 3;
	private int mode;

	private JToolBar toolBar;
		private JButton btnAdd, btnCommit, btnDelete, btnFirst, btnLast, btnHelp,
				btnNext, btnNextForm, btnPickup, btnRefresh, btnRollback,
				btnSearch, btnPrevious;
		private JLabel lbPodgrupa = new JLabel("Šifra podgrupe:");
		private JLabel lbPodgrupaNaziv = new JLabel("Naziv podgrupe:");
		private JLabel lbSifraGrupe = new JLabel("Šifra grupe:");
		
		private JTable tblGrid;
		public TaxRateTableModel tableModel;
		
	private JTextField idTaxRate = new JTextField(5);
	private JTextField tfDate = new JTextField(20);
	private JLabel labelMode = new JLabel("");
	private JTextField tfNameOfTax = new JTextField(20);
	private JTextField tfTaxIdRate = new JTextField(5);
	public ColumnList columnLista;
	
	public ColumnList getColumnLista() {
		return columnLista;
	}

	public void setColumnLista(ColumnList columnLista) {
		this.columnLista = columnLista;
	}

	private JButton btnZoom = new JButton("...");
	private JTextField tfRate=new JTextField(5);;
	private JLabel lblNewLabel;
	
	public TaxRateStandardForm(ColumnList col) {
		columnLista= col;
		getContentPane().setLayout(new MigLayout("fill", "[]", "[][][][]"));
		setSize(new Dimension(800, 600));
		setTitle("Podgrupe");
		setLocationRelativeTo(MainFrame.getInstance());
		setModal(true);
		initToolbar();
		try {
			initTable();
			tblGrid.requestFocus();
			tblGrid.changeSelection(0,0,false, false);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		initGui();
		labelMode.setText("EDIT");
		setMode(MODE_EDIT);
		idTaxRate.setVisible(true);
		lbPodgrupa.setVisible(true);
		idTaxRate.setEditable(false);
		tfTaxIdRate.setEditable(false);


		if (col != null) {
			tfNameOfTax.setText((String) col.getValue("tax.name"));
			tfTaxIdRate.setText((String) col.getValue("tax.id_tax"));

			tfNameOfTax.setEditable(false);
			tfTaxIdRate.setEditable(false);

		}

		tfTaxIdRate.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				String sifraDrzave = tfTaxIdRate.getText().trim();
				try {
					tfNameOfTax.setText(Lookup.getTaxName(sifraDrzave));
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

	}

	private void initGui() {
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout(new MigLayout("", "[][][][grow]", "[][][]"));

		JPanel buttonsPanel = new JPanel();
		btnCommit = new JButton(new ImageIcon(getClass().getResource(
				"/img/commit.gif")));
		btnCommit.addActionListener(commitAction());
		btnRollback = new JButton(new ImageIcon(getClass().getResource(
				"/img/remove.gif")));
		btnRollback.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		JLabel lbPodgrupa = new JLabel("Šifra podgrupe:");
		JLabel lbPodgrupaNaziv = new JLabel("Date:");
		JLabel lbSifraGrupe = new JLabel("ID Tax:");

		dataPanel.add(lbPodgrupa, "cell 0 0");
		dataPanel.add(idTaxRate, "cell 1 0,gapx 15px");
		
		lblNewLabel = new JLabel("Tax Rate");
		dataPanel.add(lblNewLabel, "cell 2 0,alignx trailing");
		
		//tfTaxRate = new JTextField();
		dataPanel.add(tfRate, "cell 3 0,growx");
		tfRate.setColumns(10);
		dataPanel.add(lbPodgrupaNaziv, "cell 0 1");
		dataPanel.add(tfDate, "cell 1 1 3 1,gapx 15px");
		dataPanel.add(lbSifraGrupe, "cell 0 2");
		dataPanel.add(tfTaxIdRate, "cell 1 2,gapx 15px");
		dataPanel.add(btnZoom, "cell 2 2");

		btnZoom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TaxStandardForm tsf = new TaxStandardForm();
				tsf.setVisible(true);
				ColumnList columnList = tsf.getColumnLista();
				if (columnList != null) {
					tfNameOfTax.setText((String) columnList
							.getValue("name"));
					tfTaxIdRate.setText((String) columnList
							.getValue("id"));
				}
			}
		});
		
		
		
		labelMode.setFont(new Font("Tahoma", Font.BOLD, 30));
		getContentPane().add(labelMode, "cell 0 1,alignx center");

		dataPanel.add(tfNameOfTax, "cell 3 2,pushx ");
		tfNameOfTax.setEditable(false);
		bottomPanel.add(dataPanel);

		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel, "dock east");

		getContentPane().add(bottomPanel, "cell 0 2,grow");

	}

	private void initToolbar() {
		toolBar = new JToolBar();

		btnSearch = new JButton(new ImageIcon(getClass().getResource(
				"/img/search.gif")));

		btnSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnSearch);

		btnRefresh = new JButton(new ImageIcon(getClass().getResource(
				"/img/refresh.gif")));

		btnRefresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnRefresh);

		btnPickup = new JButton(new ImageIcon(getClass().getResource(
				"/img/zoom-pickup.gif")));
		btnPickup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnPickup);

		btnHelp = new JButton(new ImageIcon(getClass().getResource(
				"/img/help.gif")));
		btnHelp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnHelp);

		toolBar.addSeparator();

		btnFirst = new JButton(new ImageIcon(getClass().getResource(
				"/img/first.gif")));
		btnFirst.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnFirst);

		btnPrevious = new JButton(new ImageIcon(getClass().getResource(
				"/img/prev.gif")));
		btnPrevious.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		toolBar.add(btnPrevious);

		btnNext = new JButton(new ImageIcon(getClass().getResource(
				"/img/next.gif")));
		btnNext.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		toolBar.add(btnNext);

		btnLast = new JButton(new ImageIcon(getClass().getResource(
				"/img/last.gif")));
		btnLast.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		toolBar.add(btnLast);

		toolBar.addSeparator();

		btnAdd = new JButton(new ImageIcon(getClass().getResource(
				"/img/add.gif")));
		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setMode(MODE_ADD);
				labelMode.setText("ADD");
				
				idTaxRate.requestFocus();
				
				tfNameOfTax.setText("");
				tfTaxIdRate.setText("");
				idTaxRate.setText("");
				tfRate.setText("");
				tfDate.setText("");
			}
		});

		toolBar.add(btnAdd);

		btnDelete = new JButton(new ImageIcon(getClass().getResource(
				"/img/remove.gif")));
		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog.setDefaultLookAndFeelDecorated(true);
				int response = JOptionPane
						.showConfirmDialog(null, "Do you want to continue?",
								"Confirm", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.NO_OPTION) {

				} else if (response == JOptionPane.YES_OPTION) {
					int selected = tblGrid.getSelectedRow();
					try {
						tableModel.deleteRow(selected);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					tblGrid.requestFocus();
					tblGrid.changeSelection(tableModel.getRowCount()-1,tableModel.getRowCount()-1,false, false);
					
				} else if (response == JOptionPane.CLOSED_OPTION) {

				}

			}
		});
		toolBar.add(btnDelete);

		toolBar.addSeparator();

		btnNextForm = new JButton(new ImageIcon(getClass().getResource(
				"/img/nextform.gif")));
		btnNextForm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		toolBar.add(btnNextForm);

		btnPickup.setEnabled(false);
		getContentPane().add(toolBar, "dock north");

	}

	private void initTable() throws SQLException {
		tblGrid= new JTable();
		JScrollPane scrollPane = new JScrollPane(tblGrid);
		getContentPane().add(scrollPane, "cell 0 0,grow");

		String[] column_names = AnnotationHelper.getColumnNames(TaxRate.class);
		String table_name = AnnotationHelper.getTableName(TaxRate.class);
		String id_column_name = AnnotationHelper.getId(TaxRate.class);
		tableModel = new TaxRateTableModel(column_names, 0, table_name, id_column_name);
		tblGrid.setModel(tableModel);
		if (columnLista != null) {
			//tableModel.openAsChildForm(columnLista.getWhereClause());
		} else {
			try {
				tableModel.open();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		// Dozvoljeno selektovanje redova
		tblGrid.setRowSelectionAllowed(true);
		// Ali ne i selektovanje kolona
		tblGrid.setColumnSelectionAllowed(false);

		// Dozvoljeno selektovanje samo jednog reda u jedinici vremena
		tblGrid.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		tblGrid.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent e) {
						if (e.getValueIsAdjusting())
							return;
						sync();
					}
				});

	}
	private ActionListener commitAction() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mode == MODE_EDIT) {	
					int selected = tblGrid.getSelectedRow();
					try {
						LinkedList<Data> values = new LinkedList<Data>();
					   values.add(new Data("id_tax", getTFText(tfTaxIdRate)));
						values.add(new Data("rate", getTFText(tfRate)));
						values.add(new Data("expiry_date", getTFText(tfDate)));
						tableModel.updateRow(values, selected);
						
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					tblGrid.setRowSelectionInterval(selected, selected); // TODO: check this
				} else if (mode == MODE_ADD) 
					addRow();
				 else if (mode == MODE_SEARCH) {
					//tableModel.search(tfSifra.getText(), tfName.getText()); //TODO commented out cuz idk what does it do
					//setMode(MODE_EDIT);
				}

			}

			private String getTFText(JTextField tf) {
				return tf.getText().trim();
			}
		};
	}

	private void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) {
			idTaxRate.setText("");
			tfDate.setText("");
			tfNameOfTax.setText("");
			return;
		}
		tfTaxIdRate.setVisible(true);
		lbPodgrupa.setVisible(true);
		tfTaxIdRate.setEditable(false);
		labelMode.setText("EDIT");
		setMode(MODE_EDIT);
		String currentId = (String) tblGrid.getModel().getValueAt(index, 0);
		String idOfTax = (String) tblGrid.getModel().getValueAt(index, 1);
		String nameOfRate = (String) tblGrid.getModel().getValueAt(index, 2);
		String expiredate = (String) tblGrid.getModel().getValueAt(index, 3);
		/*String nazivDrzave = (String) tblGrid.getModel().getValueAt(index, 3);*/
		idTaxRate.setText(currentId);
		tfDate.setText(expiredate);
		tfTaxIdRate.setText(idOfTax);
		tfRate.setText(nameOfRate);
		try {
			tfNameOfTax.setText(Lookup.getTaxName(idOfTax));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*tfNazivGrupe.setText(nazivDrzave);*/
	}
	private void addRow() {
		//String idSubGroup = tfPodgrupa.getText().trim();
		String rateOfTax = tfRate.getText().trim();
		String idTax = tfTaxIdRate.getText().trim();
		String date = tfDate.getText().trim();
		LinkedList<Data> values= new LinkedList<Data>();
		//values.add(new Data("id_group", name));
		values.add(new Data("id_tax", idTax));
		values.add(new Data("rate", rateOfTax));
		values.add(new Data("expiry_date", date));
		try {
			int index = tableModel.insertRow(values);
			tableModel.insertRow(tableModel.getRowCount(), Utils.linkedListToArray(index, values));
			tblGrid.requestFocus();
			tblGrid.changeSelection(tableModel.getRowCount()-1,tableModel.getRowCount()-1,false, false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void setMode(int mode) {
		this.mode = mode;
	}

}
