package model.bean;

import java.util.Date;

import annotation.Column;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.bill")
public class Invoice {

	@ID(name = "bill_number")
	@Column(name="bill_number")
	private Long id;

	@Column(name = "business_year")
	private Long business_year;
	
	@Column(name = "type")
	private Character type;
	
	@Column(name = "value_date")
	private Date value_date;
	
	@Column(name = "product_total")
	private Double product_total;
	
	@Column(name = "price_sum")
	private Double price_sum;
	
	@Column(name = "payment_on_account")
	private Character payment_on_account;

	@Column(name = "call_number")
	private Character call_number;
	
	@Column(name = "status")
	private Character status;

}
