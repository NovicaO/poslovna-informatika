package model.bean;

public class Data {
	private String name;
	private String value;
	
	public Data(String name, String value){
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	public String getValue() {
		return value;
	}
}
