package model.bean;

import annotation.Column;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.product_catalog")
public class product_catalog {

	@ID(name = "id_product")
	@Column(name="id_product")
	private Long id;

	@Column(name = "id_catalog_item")
	private Long id_catalog_item;
	
	@Column(name = "id_subgroup")
	private Long id_subgroup;
	
	@Column(name = "name")
	private String name;

}
