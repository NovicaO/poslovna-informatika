package model.bean;

import annotation.Column;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.tax")
public class Tax {

	@ID(name = "id_tax")
	@Column(name="id_tax")
	private Long id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "effective")
	private Boolean effective;

}
