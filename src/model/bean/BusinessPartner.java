package model.bean;

import annotation.Column;
import annotation.ForeignKey;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.business_partner")
public class BusinessPartner {

	@ForeignKey(name = "id_business_partner")
	@Column(name="id_business_partner")
	private Long id;
	
	@Column(name = "type")
	private String type;

}
