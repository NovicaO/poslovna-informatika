package model.bean;

import annotation.Column;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.catalog")
public class Catalog {

	@ID(name = "id_catalog")
	@Column(name="id_catalog")
	private Long id;

	@Column(name = "acting_date")
	private String acting_date;

}
