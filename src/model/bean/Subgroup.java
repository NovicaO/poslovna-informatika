package model.bean;

import annotation.Column;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.subgroup")
public class Subgroup {

	@ID(name = "id_subgroup")
	@Column(name="id_subgroup")
	private Long id;

	@Column(name = "id_group")
	private Long groupId;
	
	@Column(name = "name")
	private String name;

}
