package model.bean;

import annotation.Column;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.company")
public class Company {

	@ID(name = "id_company")
	@Column(name="id_company")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "address")
	private String address;

	@Column(name = "email")
	private String email;

	@Column(name = "bank_account")
	private String bank_account;
	
	@Column(name = "bank")
	private String bank;
	
	@Column(name = "TIN")
	private String TIN;
	
	@Column(name = "company_number")
	private String company_number;
	
	@Column(name = "activity_code")
	private String activity_code;
}
