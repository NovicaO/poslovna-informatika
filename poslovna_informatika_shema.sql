DROP DATABASE IF EXISTS `poslovna_informatika`;
CREATE DATABASE `poslovna_informatika` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `poslovna_informatika`;

#COMPANY / Preduzece 1
CREATE TABLE `company` (
  `id_company` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `bank_account` varchar(18) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,	
  `TIN` varchar(9) DEFAULT NULL,            /* Tax identification number <=> PIB*/
  `company_number` varchar(8) DEFAULT NULL, /* Maticni broj */
  `activity_code` varchar(45) DEFAULT NULL, /* Sifra delatnosti */
  PRIMARY KEY (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `company`(`name`,  `address`, `email`, `bank_account`, `bank`, `TIN`, `company_number`, `activity_code`) VALUES("Name K1", "Address K1", "Email K1", "Bank_Account K1", "Bank K1", "TIN K1", "CN K1", "Activity_Code K1");
INSERT INTO `company`(`name`,  `address`, `email`, `bank_account`, `bank`, `TIN`, `company_number`, `activity_code`) VALUES("Name K2", "Address K2", "Email K2", "Bank_Account K2", "Bank K2", "TIN K2", "CN K2", "Activity_Code K2");
INSERT INTO `company`(`name`,  `address`, `email`, `bank_account`, `bank`, `TIN`, `company_number`, `activity_code`) VALUES("Name K3", "Address K3", "Email K3", "Bank_Account K3", "Bank K3", "TIN K3", "CN K3", "Activity_Code K3");

#GROUP / Grupa
CREATE TABLE `group` (
  `id_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `group`(`name`) VALUES("Name g1");
INSERT INTO `group`(`name`) VALUES("Name g2");
INSERT INTO `group`(`name`) VALUES("Name g3");

#TAX / Porez
CREATE TABLE `tax`(
	`id_tax` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(120) NOT NULL,
    `effective` boolean NOT NULL DEFAULT 0,
    PRIMARY KEY(`id_tax`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `tax`(`name`) VALUES("Tax1");
INSERT INTO `tax`(`name`) VALUES("Tax2");
INSERT INTO `tax`(`name`, `effective`) VALUES("Tax3", 1);

#SUBGROUP / Podgrupa
CREATE TABLE `subgroup` (
  `id_subgroup` int(11) NOT NULL AUTO_INCREMENT,
  `id_group` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_subgroup`),
  FOREIGN KEY (`id_group`) 
    REFERENCES `group`(`id_group`)
      ON DELETE CASCADE,
  FOREIGN KEY (`id_tax`) 
     REFERENCES `tax`(`id_tax`)
       ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `subgroup`(`name`, `id_group`, `id_tax`) VALUES("Name sg1", 1, 3);
INSERT INTO `subgroup`(`name`, `id_group`, `id_tax`) VALUES("Name sg2", 2, 1);
INSERT INTO `subgroup`(`name`, `id_group`, `id_tax`) VALUES("Name sg3", 3, 2);

#BUSINESS PARTNER /Poslovni partner
CREATE TABLE `business_partner` (
  `id_business_partner` int(11) NOT NULL,
  `type` ENUM('BUYER', 'SUPPLIER', 'BOTH') DEFAULT NULL,
  FOREIGN KEY (`id_business_partner`) 
    REFERENCES `company`(`id_company`)
      ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `business_partner`(`id_business_partner`, `type`) VALUES(1, "BUYER");
INSERT INTO `business_partner`(`id_business_partner`, `type`) VALUES(2, "SUPPLIER");
INSERT INTO `business_partner`(`id_business_partner`, `type`) VALUES(3, "BOTH");

#CATALOG / Cenovnik
CREATE TABLE `catalog` (
  `id_catalog` int(6) NOT NULL AUTO_INCREMENT,
  `acting_date` date NOT NULL,
  PRIMARY KEY (`id_catalog`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `catalog`(`acting_date`) VALUES("2015-02-01");
INSERT INTO `catalog`(`acting_date`) VALUES("2015-02-02");
INSERT INTO `catalog`(`acting_date`) VALUES("2015-02-03");

#CATALOG ITEM / Stavka cenovnika
CREATE TABLE `catalog_item` (
  `id_catalog` int(11) NOT NULL,
  `id_catalog_item` int(6) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,2) NOT NULL DEFAULT 0,
	PRIMARY KEY (`id_catalog_item`),
		FOREIGN KEY (`id_catalog`) 
			REFERENCES `catalog`(`id_catalog`)
      ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `catalog_item`(`id_catalog`,`price`) VALUES(1,200);
INSERT INTO `catalog_item`(`id_catalog`) VALUES(2);
INSERT INTO `catalog_item`(`id_catalog`) VALUES(3);

#PRODUCT CATALOG / Katalog robe i usluga
CREATE TABLE `product_catalog`(
	`id_product` int(11) NOT NULL AUTO_INCREMENT,
    `id_catalog_item` int(11) NOT NULL,
    `id_subgroup` int(11) NOT NULL,
	`name` varchar(254) NOT NULL,
     PRIMARY KEY(`id_product`),
     FOREIGN KEY(`id_catalog_item`)
		REFERENCES `catalog_item`(`id_catalog_item`)
			ON DELETE CASCADE,
     FOREIGN KEY(`id_subgroup`)
		REFERENCES `subgroup`(`id_subgroup`)
			ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `product_catalog`(`id_catalog_item`,`id_subgroup`,`name`) VALUES(1,1,"First product");
INSERT INTO `product_catalog`(`id_catalog_item`,`id_subgroup`,`name`) VALUES(2,2,"Second product");
INSERT INTO `product_catalog`(`id_catalog_item`,`id_subgroup`,`name`) VALUES(3,3,"Third product");
INSERT INTO `product_catalog`(`id_catalog_item`,`id_subgroup`,`name`) VALUES(1,3,"Forth product");
INSERT INTO `product_catalog`(`id_catalog_item`,`id_subgroup`,`name`) VALUES(2,1,"Fifth product");

#TAX RATE /Poreska stopa
CREATE TABLE `tax_rate`(
	`id_tax_rate` int(11) NOT NULL AUTO_INCREMENT,
    `id_tax` int(11) NOT NULL,
    `rate` decimal(5,2) NOT NULL,
    `expiry_date` date NOT NULL,
	PRIMARY KEY(`id_tax_rate`),
    FOREIGN KEY(`id_tax`)
		REFERENCES `tax`(`id_tax`)
			ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;		
INSERT INTO `tax_rate`(`id_tax`, `rate`, `expiry_date`) VALUES(1, 3.14, "2012-01-02");   
INSERT INTO `tax_rate`(`id_tax`, `rate`, `expiry_date`) VALUES(2, 2.71, "2013-03-04");   
INSERT INTO `tax_rate`(`id_tax`, `rate`, `expiry_date`) VALUES(3, 1.23, "2014-05-06");    

#BUSINESS YEAR  / Poslovna godina
CREATE TABLE `business_year`(
	`year` int(4) NOT NULL,
    `concluded` boolean NOT NULL,
    PRIMARY KEY(`year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `business_year` (`year`, `concluded`) VALUES (2014, false);
INSERT INTO `business_year` (`year`, `concluded`) VALUES (2015, false);
INSERT INTO `business_year` (`year`, `concluded`) VALUES (2016, true);

#BILL / Izlazna faktura
CREATE TABLE `bill`(
	`bill_number` int(11) NOT NULL,
    `business_year` int(4) NOT NULL,
    `type` char(1) NOT NULL,
    `bill_date` date NOT NULL,
    `value_date` date not null,
    `invoice_date` date,
    `product_total` decimal(15,2) NOT NULL,
    `discount_total`decimal(15,2) NOT NULL,
    `tax_total` decimal(15,2) NOT NULL,
    `price_sum` decimal(15,2) NOT NULL,
    `payment_on_account` char(30) NOT NULL,
    `call_number` varchar(20),
    `status` char(1) NOT NULL,
	PRIMARY KEY(`bill_number`, `business_year`),
    FOREIGN KEY(`business_year`)
		REFERENCES `business_year`(`year`)
			ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#BILL ITEMS / Stavke fakture
CREATE TABLE `bill_items`(
	`item_number` int(5) NOT NULL,
    `units` varchar(15) NOT NULL,
    `amount` decimal(15,2) NOT NULL,
    `bill_number` int(11) NOT NULL,
    `business_year` int(4) NOT NULL,
    PRIMARY KEY(`item_number`),
    FOREIGN KEY(`bill_number`, `business_year`)
		REFERENCES `bill`(`bill_number`, `business_year`)
			ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;







  
    
	